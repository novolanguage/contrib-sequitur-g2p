FROM debian
MAINTAINER david@novolanguage.com
RUN apt-get update && apt-get install -y --no-install-recommends swig g++ python-dev python-numpy
ADD . /tmp/sequitur-g2p
# RUN cd tmp/sequitur-g2p; python setup.py install
RUN cd tmp/sequitur-g2p && python setup.py build && python setup.py install
ENTRYPOINT ["/usr/local/bin/g2p.py"]
